import axios from 'axios';

export const getRaces = () => axios.get('/api/races').then(response => response.data.data);

export const getPonies = (raceId: number) => axios.get(`/api/races/${raceId}/ponies`).then(response => response.data.data);
