import * as CONSTANTS from '@/utils/constants';

export const getRandomColor = (): string => {
  const colors = Object.values(CONSTANTS.PONY_COLORS);
  return colors[colors.length * Math.random() || 0];
};
