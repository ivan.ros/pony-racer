const path = require('path');
const fs = require('fs');
const responsesPath = path.resolve(__dirname, './responses/');

let files = fs.readdirSync(responsesPath);
let obj = {};

function isJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

console.log('📂 Loading JSON files...\n');
files.forEach(file => {
  if (file.indexOf('.json') > -1) {
    const fileName = file.split('.')[0];
    const filePath = path.resolve(__dirname, './responses/' + file);

    if (isJson(fs.readFileSync(filePath))) {
      console.log('- ' + file);
      const fileData = require(filePath).data;
      obj = { ...obj, [fileName]: fileData };
    }
  }
});

const objOrdered = {};
Object.keys(obj)
  .sort()
  .forEach(function (key) {
    objOrdered[key] = obj[key];
  });

module.exports = objOrdered;
