const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router(require('./server-db.js'));
const port = process.env.PORT || 8081;

server.use(jsonServer.defaults());
server.use(jsonServer.bodyParser);

server.use(
  jsonServer.rewriter({
    '/api/*': '/$1',
    '/races': '/api/races',
    '/api/races/:raceId/ponies': '/api/ponies'
  })
);

server.put('*', (req, res) => {
  res.send({ data: req.body });
});

server.post('*', (req, res) => {
  res.send({ data: req.body });
});

server.patch('*', (req, res) => {
  res.send({ data: req.body });
});

server.delete('*', (req, res) => {
  res.send({ data: {} });
});

server.use('/api', router);

router.render = function (req, res) {
  if (req.method === 'GET') {
    res.jsonp({ data: res.locals.data });
  }
};

server.listen(port, () => {
  console.log(`\n✅ JSON-Server is running on http://localhost:${port}\n`);
});
