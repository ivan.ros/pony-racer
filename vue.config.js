module.exports = {
  configureWebpack: {
    devtool: 'source-map'
  },
  devServer: {
    proxy: {
      '^/api': {
        target: 'http://localhost:8081/api',
        ws: true,
        changeOrigin: true
      }
    }
  }
};
